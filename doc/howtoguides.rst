How-to Guides
=============

*Ewoks* is extremely versatile with respect to workflow edition and execution. This section presents several possible ways of handling workflows with *Ewoks*.

.. toctree::

    howtoguides/running_workflows
    howtoguides/python
    howtoguides/job
    howtoguides/gui
    howtoguides/rest
    howtoguides/benchmark
    howtoguides/ewoks_events
    howtoguides/task_python
