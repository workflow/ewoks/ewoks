Use Ewoks graphical interfaces
==============================

Ewoks comes with two graphical interfaces. You can either of the two to create and edit workflows.

``ewoksweb``: full-stack web application
----------------------------------------

Install ``ewoksweb``

.. code:: bash

    pip install ewoksweb

Start ``ewoksweb``

.. code:: bash

    ewoksweb

``ewoksweb`` will create folders to store the tasks/workflows that you will create when using it.

For more information see the `ewoksweb documentation <https://ewoksweb.readthedocs.io/>`_.

``ewoks-canvas``: the Orange desktop graphical interface
--------------------------------------------------------

Install requirements

.. code:: bash

    pip install ewoksorange[orange]

Launch the graphical interface

.. code:: bash

    ewoks-canvas

For more information see the `ewoksorange documentation <https://ewoksorange.readthedocs.io/>`_.
