Set up a REST Ewoks server
==========================

Install requirements

.. code:: bash

    pip install ewoksserver

Start the REST server

.. code:: bash

    ewoks-server

or for an installation with the system python

.. code:: bash

    python3 -m ewoksserver.server

For more information see the `ewoksserver documentation <https://ewoksserver.readthedocs.io/>`_.
